# vue-interactive-graphics (geometry?)


Vue-interactive-graphics component allows to manipulate easily geometric objects by drag and two way data-binding. Under the hood, it uses [d3.js](http://https://d3js.org/).

## usage

```html
<InteractiveGraphics
  :width="800"
  :height="400"
  :dataString="dataText"
  @update="dataUpdate"
/>
```

 - **width, height:** (numbers), dimentions in pixels of the component
 - **dataString:** (string), JSON object as a string representing the data
 - **dataUpdate** ( fun: string -> void ) update function called when vue-interactive-graphics modifies some values in the data (when the user drags some element)

## data structure

data passed is a JSON object given to vue-interactive-graphics as a string and is
composed by the following elements:

 - **instr**: List of instructions (Line, Circle, etc.). All of them are listed below.
 - **layerOrder**: Indicates layers to display and in which order
 - **drawSize**: size of the drawing area (indicated by fields *w* and *h*).
 - **background**: (OPTIONAL) background color, by default white.

### Line Instruction

Line instructions have the following fields:

 - **x1,y1,x2,y2**: coordinates of the first point and second point as in the SVG spec.
 - **stroke**: (OPTIONAL) color of the line (by default: '#ccc')
 - **stroke-width**: (OPTIONAL) width of the line (by default 1)

### Circle instruction

Circle instruction have the following fields:

 - **cx,cy,r**: center and radius of the circle
 - **stroke**: (OPTIONAL) color of the line (by default: '#ccc')
 - **stroke-width**: (OPTIONAL) width of the line (by default 1)
 - **fill**: (OPTIONAL) filling of the area (default: 'black')
 - **text**: (OPTIONAL) text at the center of the circle
 - **textColor**: (OPTIONAL) color of text
 - **textStroke**: (OPTIONAL) border color of the text
 - **draggable**: (OPTIONAL) allows user to drag the shape


### Rectangle instruction

Rectangle instruction have the following fields:

 - **x,y,width,height**: coordinates and size of the rectangle
 - **stroke**: (OPTIONAL) color of the line (by default: '#ccc')
 - **stroke-width**: (OPTIONAL) width of the line (by default 1)
 - **fill**: (OPTIONAL) filling of the area (default: 'black')
 - **text**: (OPTIONAL) text at the center of the circle
 - **textColor**: (OPTIONAL) color of text
 - **textStroke**: (OPTIONAL) border color of the text
 - **draggable**: (OPTIONAL) allows user to drag the shape
 - **resize**: (OPTIONAL) allows user to resize rectangle. Possible values are:
    - **constantSurface**: surface stay the same as user resizes the shape.


### Grid instruction

Grid instruction have the following fields:

 - **dim-x, dim-y**: size of the grid mesh
 - **x,y,w,h**: coordinates and size of the grid
 - **stroke**: (OPTIONAL) color of the line (by default: '#ccc')
 - **stroke-width**: (OPTIONAL) width of the line (by default 1)
 - **fill**: (OPTIONAL) filling of the lines (default: 'black')


### Rounded path

 - **radius**: radius of turns
 - **width**: width of the stroke
 - **instructions**: list of x,y coordinates of the drawing points

### array
 - **dim-x**: width of tile
 - **dim-y**: height of tile
 - **array**: array of array of booleans
 - **x,y**: position of array
 - **stroke**: (OPTIONAL) color of the line (by default: '#aaa')
 - **stroke-width**: (OPTIONAL) width of the line (by default 0)
 - **fill**: (OPTIONAL) filling of the area (default: 'black')

## events

 - **update**: emited when the user changes something in the drawing and returns new drawing instructions
 - **change**: emited when the user changes something in the drawing and returns a json object containing:
    - **id**: id of the instruction (position in the instruction array)
    - **changes**: dict with values (*k,v*) where *k* is the key and *v* the new value

## principles

 - **layers :** vue-interactive-graphics is given a list of layers. Each layer can be interpreted as a group element (g) in SVG. Each layer contains geometric elements like lines, rectangles, circles, text etc.
 - **geometric element :** Each geometric element is composed of display properties according to its type
 and coordinates.
 - **scaling factor :**  According to component size and drawing size, dimentions are scaled.



# TODO

- mobile support
- add click event
- add scenarii
- firefox multiple selection compatibility
- bug add instruction (grid case)