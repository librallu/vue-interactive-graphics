var webpack = require('webpack')
var merge = require('webpack-merge')
var path = require('path')
var UglifyJSPlugin = require('uglifyjs-webpack-plugin')

var plugins = []
var env = process.env.WEBPACK_ENV

var externals = {}

if ( env === 'prod' ) {
  var UglifyJsPlugin = UglifyJSPlugin

  plugins.push(new UglifyJsPlugin({
    sourceMap: false
  }))
  plugins.push(new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: '"prod"'
    }
  }))
  externals = {
    d3: 'd3',
    vue: 'vue',
    vueDecorator: 'vue-property-decorator'
  }
}

var commonConfig = {
  externals: externals,
  devtool: 'source-map',
  module: {
    loaders: [
      { test: /\.vue$/, loader: 'vue-loader' },
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          appendTsSuffixTo: [/\.vue$/]
        }
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        loader: "file-loader",
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader", options: { sourceMap: true } },
          { loader: "sass-loader", options: { sourceMap: true } }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  plugins
}

module.exports = [
  merge(commonConfig, {
    entry: path.resolve(__dirname + '/src/main.ts'),
    output: {
      filename: './build/vue-interactive-graphics-demo.js'
    },
    watchOptions: {
      poll: true
    },
    devServer: {
      inline: true
    },
    devtool: '#eval-source-map'
  }),
  merge(commonConfig, {
    entry: path.resolve(__dirname + '/src/wrapperBuild.ts'),
    output: {
      path: path.resolve(__dirname, './build'),
      publicPath: '/build/',
      filename: 'vue-interactive-graphics.min.js',
      libraryTarget: 'umd',
      library: 'vue-interactive-graphics',
      umdNamedDefine: true
    }
  })
]
