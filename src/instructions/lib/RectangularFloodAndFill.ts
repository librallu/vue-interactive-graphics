/*
  RECTANGULAR FLOOD AND FILL
*/

interface RectangleMap {
  x: number,
  y: number,
  w: number,
  h: number
}

interface Coord {
  x: number,
  y: number
}


function _choose(t:boolean[][]):Coord {
  let res = {x:-1,y:-1}
  for ( let i = 0 ; i < t.length ; i++ ) {
    for ( let j = 0 ; j < t[i].length ; j++ ) {
      if ( t[i][j] )
        return {x:j,y:i}
    }
  }
  return res
}


function _grow(t:boolean[][], p:Coord):RectangleMap {
  let res = {x:p.x,y:p.y,w:1,h:1}
  t[p.y][p.x] = false
  let cont = true

  while ( cont ) {
    cont = false
    // grow top
    if ( res.y > 0 ) {
    let ok = true
      for ( let i = res.x ; i < res.x+res.w ; i++ ) {
        if ( !t[res.y-1][i] ) ok=false
      }
      if ( ok ) {
        for ( let i = res.x ; i < res.x+res.w ; i++ ) {
          t[res.y-1][i] = false
        }
        cont = true
        res.y --
        res.h ++
      }
    }
    // grow bottom
    if ( res.y+res.h < t.length-1 ) {
    let ok = true
      for ( let i = res.x ; i < res.x+res.w ; i++ ) {
        if ( !t[res.y+res.h][i] ) ok=false
      }
      if ( ok ) {
        for ( let i = res.x ; i < res.x+res.w ; i++ ) {
          t[res.y+res.h][i] = false
        }
        cont = true
        res.h ++
      }
    }
    // grow left
    if ( res.x > 0 ) {
    let ok = true
      for ( let i = res.y ; i < res.y+res.h ; i++ ) {
        if ( !t[i][res.x-1] ) ok=false
      }
      if ( ok ) {
        for ( let i = res.y ; i < res.y+res.h ; i++ ) {
          t[i][res.x-1] = false
        }
        cont = true
        res.x --
        res.w ++
      }
    }
    // grow right
    if ( res.x+res.w < t[0].length-1 ) {
    let ok = true
      for ( let i = res.y ; i < res.y+res.h ; i++ ) {
        if ( !t[i][res.x+res.w] ) ok=false
      }
      if ( ok ) {
        for ( let i = res.y ; i < res.y+res.h ; i++ ) {
          t[i][res.x+res.w] = false
        }
        cont = true
        res.w ++
      }
    }
  }
  return res
}


export function rectangularFloodFill(t:boolean[][]):RectangleMap[] {
  let l = []
  for ( let i = 0 ; i < t.length ; i++ ) { // copy t
    let tmp = []
    for ( let j = 0 ; j < t[i].length ; j++ ) {
      tmp.push(!t[i][j])
    }
    l.push(tmp)
  }
  let res:RectangleMap[] = []
  let p = _choose(l)
  while ( p.x != -1 || p.y != -1 ) {
    res.push(_grow(l,p))
    p = _choose(l)
  }
  return res
}
