import { Drawing, IGInstruction, CLASS_ID_PREFIX } from "./Drawing"


export class LineSVG implements Drawing {

  addSVG(instr:IGInstruction, parent:any, scaleX, scaleY):void {
    return parent.append('line')
      .data([instr])
      .attr('x1', scaleX(instr.x1))
      .attr('x2', scaleX(instr.x2))
      .attr('y1', scaleY(instr.y1))
      .attr('y2', scaleY(instr.y2))
      .attr('class', 'lineDrawing')
      .classed(CLASS_ID_PREFIX+instr.id, true)
      .attr('stroke', instr['stroke'] !== 'undefined' ? instr['stroke'] : "#ccc")
      .attr('stroke-width', instr['stroke-width'] !== undefined ? instr['stroke-width'] : 1)
  }

}
