import { Drawing, IGInstruction, CLASS_ID_PREFIX } from "./Drawing"

export class GridSVG implements Drawing {

  addSVG(instr:IGInstruction, parent:any, scaleX, scaleY):void {
    instr['dim-x'] = Math.max(instr.w/200, instr['dim-x'])
    instr['dim-y'] = Math.max(instr.h/200, instr['dim-y'])
    let group = parent.append('g')
      .data([instr])
      .classed('grid', true)
      .classed(CLASS_ID_PREFIX+instr.id, true)
      .attr('stroke', 'stroke' in instr ? instr['stroke'] : "#ccc")
      .attr('stroke-width', 'stroke-width' in instr ? instr['stroke-width'] : 1)
      .attr('fill', 'fill' in instr ? instr['fill'] : "black")

    for ( let i = instr.x ; i <= instr.w+instr.x ; i += instr['dim-x'] ) {
      group.append('line')
        .attr('x1', scaleX(i))
        .attr('x2', scaleX(i))
        .attr('y1', scaleY(instr.y))
        .attr('y2', scaleY(instr.y+instr.h))
    }
    for ( let j = instr.y ; j <= instr.h+instr.y ; j += instr['dim-y'] ) {
      group.append('line')
        .attr('x1', scaleX(instr.x))
        .attr('x2', scaleX(instr.x+instr.w))
        .attr('y1', scaleY(j))
        .attr('y2', scaleY(j))
    }
    return group
  }

}
