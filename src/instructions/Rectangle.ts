import { Drawing, IGInstruction, styleText, CLASS_ID_PREFIX } from "./Drawing"


export class RectangleSVG implements Drawing {

  addSVG(instr:IGInstruction, parent:any, scaleX, scaleY):void {
    let group = parent.append('g')
      .data([instr])
      .classed('rectDrawing', true)
      .classed('draggable', "draggable" in instr)
      .classed('resizeConstantSurface', "resize" in instr && instr.resize === "constantSurface")
      .classed(CLASS_ID_PREFIX+instr.id, true)
      group.append('rect')
        .attr('x', scaleX(instr.x))
        .attr('y', scaleY(instr.y))
        .attr('width', scaleX(instr.width))
        .attr('height', scaleY(instr.height))
        .classed('representation', true)
        .attr('stroke', instr['stroke'] !== 'undefined' ? instr['stroke'] : "#ccc")
        .attr('stroke-width', instr['stroke-width'] !== undefined ? instr['stroke-width'] : 1)
        .attr('fill', instr['fill'] !== undefined ? instr['fill'] : "black")
        .classed('representation', true)
      if ( "text" in instr ) {
        styleText(instr, group.append('text')
          .text(instr.text)
          .attr('x', scaleX(instr.x+instr.width/2))
          .attr('y', scaleY(instr.y+instr.height/2))
        )
      }
      return group
  }

}
