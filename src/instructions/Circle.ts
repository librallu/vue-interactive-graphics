import { Drawing, IGInstruction, styleText, CLASS_ID_PREFIX } from "./Drawing"

export class CircleSVG implements Drawing {

  addSVG(instr:IGInstruction, parent:any, scaleX, scaleY):void {
    let group = parent.append('g')
      .data([instr])
      .classed('circleDrawing', true)
      .classed('draggable', "draggable" in instr)
      .classed(CLASS_ID_PREFIX+instr.id, true)
    group.append('ellipse')
        .attr('cx', scaleX(instr.cx) )
        .attr('rx', scaleX(instr.r) )
        .attr('cy', scaleY(instr.cy) )
        .attr('ry', scaleY(instr.r) )
        .attr('stroke', instr['stroke'] !== 'undefined' ? instr['stroke'] : "#ccc")
        .attr('stroke-width', instr['stroke-width'] !== undefined ? instr['stroke-width'] : 1)
        .attr('fill', instr['fill'] !== undefined ? instr['fill'] : "black")
        .classed('representation', true)
    if ( "text" in instr ) {
      styleText(instr, group.append('text')
        .text(instr.text)
        .attr('x', scaleX(instr.cx))
        .attr('y', scaleY(instr.cy))
      )
    }
    return group
  }

}
