import { Drawing, IGInstruction, CLASS_ID_PREFIX } from "./Drawing"
import * as d3 from "d3"


export class RoundedPathSVG implements Drawing {
  pathToRounded(path:{x:number,y:number}[], r:number) {
    let res = []
    let epsilon = 0.00001;

    for ( var i = 0 ; i < path.length-2 ; i++ ) {
      let a = path[i];
      let b = path[i+1];
      let c = path[i+2];
      let pi = Math.PI

      if ( b.x-a.x > epsilon ) { // horizontal right channel
        let tmp = (i==0)?a.x:a.x+r;
        res.push({type:'seg', x1:tmp, y1:a.y, x2:b.x-r, y2:b.y});
        // 2 options, turn up or down
        if ( b.y-c.y > epsilon ) { // up channel
          res.push({type:'arc', x:b.x-r, y:b.y-r, s_angle:pi/2, e_angle:pi});
        } else { // down channel
          res.push({type:'arc', x:b.x-r, y:b.y+r, s_angle:0, e_angle:pi/2});
        }
      } else if ( a.x-b.x > epsilon ) { // horizontal left channel
        let tmp = (i==0)?a.x:a.x-r;
        res.push({type:'seg', x1:tmp, y1:a.y, x2:b.x+r, y2:b.y});
        // 2 options, turn up or down
        if ( b.y-c.y > epsilon ) { // up channel
          res.push({type:'arc', x:b.x+r, y:b.y-r, s_angle:pi, e_angle:3*pi/2});
        } else { // down channel
          res.push({type:'arc', x:b.x+r, y:b.y+r, s_angle:0, e_angle:-pi/2});
        }
      } else if ( a.y-b.y > epsilon ) { // vertical top channel
        let tmp = (i==0)?a.y:a.y-r;
        res.push({type:'seg', x1:a.x, y1:tmp, x2:b.x, y2:b.y+r});
        // 2 options, turn up or down
        if ( b.x-c.x > epsilon ) { // left channel
          res.push({type:'arc', x:b.x-r, y:b.y+r, s_angle:0, e_angle:pi/2});
        } else { // right channel
          res.push({type:'arc', x:b.x+r, y:b.y+r, s_angle:3*pi/2, e_angle:4*pi/2-epsilon});
        }
      } else if ( b.y-a.y > epsilon ) { // vertical bottom channel
        let tmp = (i==0)?a.y:a.y+r;
        res.push({type:'seg', x1:a.x, y1:tmp, x2:b.x, y2:b.y-r});
        // 2 options, turn up or down
        if ( b.x-c.x > epsilon ) { // left channel
          res.push({type:'arc', x:b.x-r, y:b.y-r, s_angle:pi/2, e_angle:pi});
        } else { // right channel
          res.push({type:'arc', x:b.x+r, y:b.y-r, s_angle:pi, e_angle:3*pi/2});
        }
      }
    }

    let a = path[path.length-2];
    let b = path[path.length-1];
    // add last segment
    if ( b.x-a.x > epsilon ) {
      res.push({type:'seg', x1:a.x+r, x2:b.x, y1:a.y, y2:b.y});
    } else if ( a.x-b.x > epsilon ) {
      res.push({type:'seg', x1:a.x-r, x2:b.x, y1:a.y, y2:b.y});
    } else if ( a.y-b.y > epsilon ) {
      res.push({type:'seg', x1:a.x, x2:b.x, y1:a.y-r, y2:b.y});
    } else if ( b.y-a.y > epsilon ) {
      res.push({type:'seg', x1:a.x, x2:b.x, y1:a.y+r, y2:b.y});
    }

    return res
  }

  addSVG(instr:IGInstruction, parent:any, scaleX, scaleY):void {
    let g = parent.append('g')
      .classed('roundedPath', true)
      .classed(CLASS_ID_PREFIX+instr.id, true)
      .attr('stroke', 'stroke' in instr ? instr['stroke'] : "#ccc")
      .attr('fill', 'fill' in instr ? instr['fill'] : "#ccc")

    this.pathToRounded(instr.instructions, instr.radius).forEach( (e) => {
      switch(e.type) {
        case "seg":
            g.append('line')
              .attr('x1', e.x1)
              .attr('x2', e.x2)
              .attr('y1', e.y1)
              .attr('y2', e.y2)
              .attr('stroke-width', instr.width)
              .attr('transform', `scale(${scaleX(1)},${scaleY(1)})`)
          break
        case "arc":
          g.append('path')
            .attr("d", d3.arc()
              .innerRadius(instr.radius-instr.width/2)
              .outerRadius(instr.radius+instr.width/2)
              .startAngle(e['s_angle'])
              .endAngle(e['e_angle'])
            ).attr("transform", `translate(${scaleX(e.x)},${scaleY(e.y)}) scale(${scaleX(1)},${scaleY(1)})`)
            .attr('stroke-width', 0)
          break
      }
    })
    return g
  }

}
