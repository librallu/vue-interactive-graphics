export interface IGInstruction {
  layer: string,
  type: string,
  draggable: boolean,
  [propName: string]: any
}

export const CLASS_ID_PREFIX = "vueig-id-"

export function styleText(instr, obj) {
  return obj.attr("font-family", "sans-serif")
    .attr("font-size", "1em")
    .attr("text-anchor", "middle")
    .attr('alignment-baseline', 'central')
    .attr('fill', ("textColor" in instr) ? instr.textColor : '#444')
    .attr('stroke', ("textStroke" in instr) ? instr.textStroke : '#ccc')
    .attr('stroke-width', 0.5)
    .attr('pointer-events', 'none')
}


export interface Drawing {

  addSVG(instr:IGInstruction, parent:any, scaleX, scaleY):any

}
