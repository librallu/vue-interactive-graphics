import { Drawing, IGInstruction, CLASS_ID_PREFIX } from "./Drawing"
import { rectangularFloodFill } from "./lib/RectangularFloodAndFill"

/**
  dim-x: width of tile
  dim-y: height of tile
  array: array of array of booleans
  x,y: position of array
*/
export class ArraySVG implements Drawing {

  addSVG(instr:IGInstruction, parent:any, scaleX, scaleY):void {
    let group = parent.append('g')
    .data([instr])
      .classed('array', true)
      .classed(CLASS_ID_PREFIX+instr.id, true)
      .attr('stroke', 'stroke' in instr ? instr['stroke'] : "#aaa")
      .attr('stroke-width', 'stroke-width' in instr ? instr['stroke-width'] : 0)
      .attr('fill', 'fill' in instr ? instr['fill'] : "black")
    rectangularFloodFill(instr.array).forEach( (e) => {
      group.append('rect')
        .attr('x', scaleX(instr['dim-x']*e.x))
        .attr('y', scaleY(instr['dim-y']*e.y))
        .attr('width', scaleX(instr['dim-x']*e.w))
        .attr('height', scaleY(instr['dim-y']*e.h))

    })
    return group
  }

}
