import * as d3 from "d3"


export function applyClickHandler(self, obj, scaleX, scaleY, fun) {
    obj.on('click', function() {
        let evx = d3.mouse(this)[0]
        let evy = d3.mouse(this)[1]
        fun(scaleX.invert(evx), scaleY.invert(evy))
    })
}