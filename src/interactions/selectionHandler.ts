import * as d3 from "d3"

export function removeSelectionBox(obj) {
    obj.select('.multipleSelection').remove()
    obj.select('.multipleSelectionRect').remove()
}

export function applySelectionHandler(self, obj, scaleX, scaleY) {
    let stickingResolution = 0.01
    let stickingOffset = 0

    obj.on("contextmenu", function(d,i) {
        d3.event.preventDefault()
    })

    let selection = {
        x1:0,
        y1:0,
        x2:0,
        y2:0
    }

    enum State {
        Idle,
        Moving
    }

    let s = State.Idle
    let prevState = {x:0,y:0}
    
    obj.on('mousedown', function() {
        let e = d3.event
        if ( e.which == 3 ) {
            removeSelectionBox(obj)
            let coords = d3.mouse(this)
            switch(s) {
                case State.Idle: // start selection rectangle
                    s = State.Moving
                    prevState = {x:coords[0], y:coords[1]}
                    obj.append('rect')
                    .attr('x', 0).attr('y',0).attr('width',0).attr('height',0)
                    .classed('multipleSelection', true)
                    .attr('stroke', '#000')
                    .attr('fill', '#8d4')
                    .attr('opacity', 0.3)
                    break
            }
        }
    })

    obj.on('mouseup', function() {
        let instructions = self.instr.instr
        let e = d3.event
        if ( e.which == 3 ) {
            let coords = d3.mouse(this)
            switch(s) {
                case State.Moving:
                    s = State.Idle
                    removeSelectionBox(obj)
                    let newSelection = []
                    let x1 = Math.min(prevState.x, coords[0])
                    let x2 = Math.max(prevState.x, coords[0])
                    let y1 = Math.min(prevState.y, coords[1])
                    let y2 = Math.max(prevState.y, coords[1])
                    for ( let d of instructions ) {
                        switch(d.type) {
                            case 'rect':
                                let midx = scaleX(d.x+d.width/2)
                                let midy = scaleY(d.y+d.height/2)
                                if ( midx >= x1 && midx <= x2 && midy >= y1 && midy <= y2) {
                                    newSelection.push(d)
                                }
                            break
                            case 'circle':
                                if ( scaleX(d.cx) >= x1 && scaleX(d.cx) <= x2 && scaleY(d.cy) >= y1 && scaleY(d.cy) <= y2) {
                                    newSelection.push(d)
                                }
                            break
                        }
                    }
                    if ( newSelection.length > 0 ) {
                        self.setSelectedInstructions(newSelection)
                    }
                    break
            }
        }
    })

    obj.on('mousemove', function() {
        let e = d3.event
        if (s == State.Moving && e.which == 3) {
            let coords = d3.mouse(this)
            obj.select('.multipleSelection')
            .attr('x', Math.min(prevState.x, coords[0]))
            .attr('y', Math.min(prevState.y, coords[1]))
            .attr('width', Math.abs(
                Math.min(prevState.x, coords[0])-Math.max(prevState.x, coords[0])
            ))
            .attr('height', Math.abs(
                Math.min(prevState.y, coords[1])-Math.max(prevState.y, coords[1])
            ))
        }
    })
    
}