import * as d3 from "d3"
import { INSTRUCTION_ID } from "../constants"

export function applyResizeConstantSurfaceHandler(self, obj, sx, sy) {

  let g:any = self.svgd3.select('.everything').node()

  let widthHandles = 10

  let dragVertical = function(up) {
    return d3.drag().on('start', function(d:any,i) {
      d.surface = d.width*d.height
      let ev = d3.event.y
      d.start = d.y+(!up ? d.height : 0)
      d.startHeight = d.height
    }).on('drag', function(d:any,i) {
      let ev = sy.invert(d3.mouse(g)[1])
      d.height = Math.max(0.1, d.startHeight+((!up?-1:1)*d.start+(up?-1:1)*ev))
      if ( up ) d.y = ev
      d.width = d.surface/d.height
      let tab = [{k:'height', v:d.height}, {k:'width', v:d.width}]
      if ( up ) tab.push({k:'y', v: d.y})
      self.modifyInstruction(d[INSTRUCTION_ID], tab)
    })
  }

  let dragHorizontal = function(left) {
    return d3.drag().on('start', function(d:any,i) {
      d.surface = d.width*d.height
      let ev = d3.event.x
      d.start = d.x+(!left ? d.width : 0)
      d.startWidth = d.width
    }).on('drag', function(d:any,i) {
      let ev = sx.invert(d3.mouse(g)[0])
      d.width = Math.max(0.1, d.startWidth+((!left?-1:1)*d.start+(left?-1:1)*ev))
      if ( left ) d.x = ev
      d.height = d.surface/d.width
      let tab = [{k:'width', v:d.width}, {k:'height', v:d.height}]
      if ( left ) tab.push({k:'x', v: d.x})
      self.modifyInstruction(d[INSTRUCTION_ID], tab)
    })
  }

  // add dragHandles
  let topHandle = obj.append('rect') // top handle
    .attr('x', function(d) { return sx(d.x+d.width/4) })
    .attr('y', function(d) { return sy(d.y)-widthHandles/2 })
    .attr('width', function(d) { return sx(d.width/2) })
    .attr('height', widthHandles)
    .attr('fill', '#aaf9')
    .attr("cursor", "ns-resize")
    .classed('topHandle', true)
  dragVertical(true)(topHandle)

  let bottomHandle = obj.append('rect') // bottom handle
    .attr('x', function(d) { return sx(d.x+d.width/4) })
    .attr('y', function(d) { return sy(d.y+d.height)-widthHandles/2 })
    .attr('width', function(d) { return sx(d.width/2) })
    .attr('height', widthHandles)
    .attr('fill', '#aaf9')
    .attr("cursor", "ns-resize")
    .classed('bottomHandle', true)
  dragVertical(false)(bottomHandle)

  let leftHandle = obj.append('rect') // left handle
    .attr('x', function(d) { return sx(d.x)-widthHandles/2 })
    .attr('y', function(d) { return sy(d.y+d.height/4) })
    .attr('width', widthHandles)
    .attr('height', function(d) { return sy(d.height/2) })
    .attr('fill', '#aaf9')
    .attr("cursor", "ew-resize")
    .classed('leftHandle', true)
  dragHorizontal(true)(leftHandle)

  let rightHandle = obj.append('rect') // right handle
    .attr('x', function(d) { return sx(d.x+d.width)-widthHandles/2 })
    .attr('y', function(d) { return sy(d.y+d.height/4) })
    .attr('width', widthHandles)
    .attr('height', function(d) { return sy(d.height/2) })
    .attr('fill', '#aaf9')
    .attr("cursor", "ew-resize")
    .classed('rightHandle', true)
  dragHorizontal(false)(rightHandle)
}
