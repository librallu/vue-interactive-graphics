import * as d3 from "d3"
import { INSTRUCTION_ID } from "../constants"

/**
 * 
 * @param v value to round
 * @param p precision
 */
function round(v:number,p:number):number {
  return Math.round(v/p)*p
}

/**
 * 
 * @param self vue component
 * @param obj svg object to apply listener
 * @param scaleX D3 scale (X)
 * @param scaleY D3 scale (Y)
 * @param stickingResolution the center sticks to a multiple of this value
 */
export function applyDragHandler(self, obj, scaleX, scaleY) {
  let g:any = self.svgd3.select('.everything').node()
  let stickingResolution = 0.01

  obj.each(function(p, j) {
    let repr = d3.select(this)
    repr.attr("cursor", "move")  

    repr.call(d3.drag()
      .on('start', function(d:any) {
        let evx = d3.mouse(g)[0]
        let evy = d3.mouse(g)[1]
        switch(d.type) {
          case "circle":
            d.startX = scaleX(d.cx)-evx
            d.startY = scaleY(d.cy)-evy
            break
          case "rect":
            d.startX = scaleX(d.x-d.width/2)-evx
            d.startY = scaleY(d.y-d.height/2)-evy
            break
        }
      })
      .on('drag', function(d:any) {
        let evx = d3.mouse(g)[0]
        let evy = d3.mouse(g)[1]
        let resolution = stickingResolution        
        if ( d.stickingResolution )
          resolution = d.stickingResolution
        let offset = 0
        if ( d.stickingOffset )
          offset = d.stickingOffset
        switch(d.type) {
          case "circle":
            self.modifyInstruction(d[INSTRUCTION_ID], [
              {k:'cx', v: round(scaleX.invert(evx+d.startX),resolution)+offset}, 
              {k:'cy', v:round(scaleY.invert(evy+d.startY),resolution)+offset}
            ])
            break
          case "rect":
            self.modifyInstruction(d[INSTRUCTION_ID], [
              {k:'x', v: round(scaleX.invert(evx+d.startX),resolution)+d.width/2+offset}, 
              {k:'y', v:round(scaleY.invert(evy+d.startY),resolution)+d.height/2+offset}
            ])
            break
        }
      })
      .on('end', function(d:any) {
        d3.select(this).classed("active", false)
      })
    )

  })

}
