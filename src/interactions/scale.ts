import * as d3 from "d3"
import { zoom } from "d3";

enum ScaleType {
    Metric,
    Unit
}

let metricSizes = {
    1e6: 'nm',
    1e3: 'µm',
    1: 'mm',
    1e-3: 'm'
}

export function deleteScale(svgd3) {
    d3.select('.scaleContainer').remove()
}

export function drawScale(svgd3, scaleX, scaleY, zoomLevel:number, sizeParent:{w:number,h:number}) {
    let prefixX = 10
    let prefixY = 10
    let scaleSize = 1
    let precision = 1000
    let obj = svgd3
    let text = ""
    if ( zoomLevel > 1e6/500 ) {
        text = Math.round(1e6/zoomLevel*precision)/precision*scaleSize + " " + metricSizes[1e6]
    } else if ( zoomLevel > 1e3/500 ) {
        text = Math.round(1e3/zoomLevel*precision)/precision*scaleSize + " " + metricSizes[1e3]
    } else if ( zoomLevel > 1/500 ) {
        text = Math.round(1/zoomLevel*precision)/precision*scaleSize + " " + metricSizes[1]
    } else {
        text = Math.round(1e-3/zoomLevel*precision)/precision*scaleSize + " " + metricSizes[1e-3]
    }

    let container = obj.append('g')
        .classed('scaleContainer', true)
        .attr('transform', `translate(${prefixX},${sizeParent.h-prefixY})`)
    container.append('line')
        .attr('x1', 0)
        .attr('y1', 0)
        .attr('x2', scaleX(scaleSize))
        .attr('y2', 0)
        .attr('stroke', 'red')
        .attr('stroke-width', '2')
    container.append('line')
        .attr('x1', 0)
        .attr('y1', 0)
        .attr('x2', 0)
        .attr('y2', -scaleY(scaleSize))
        .attr('stroke', 'red')
        .attr('stroke-width', '2')
    container.append('text')
        .attr('font-family','sans-serif')
        .text(text)
        .attr('x', scaleX(0.5))
        .attr('y', -scaleY(0.5))
        .attr("font-size", "15px")
        .attr("stroke-width", "0")
}