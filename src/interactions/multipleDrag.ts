import * as d3 from "d3"
import { INSTRUCTION_ID } from "../constants"

export function createDragRectangle(self, instr, svg, scaleX, scaleY) {
    // creates a selection box
    let x1 = Infinity
    let x2 = 0
    let y1 = Infinity
    let y2 = 0
    for ( let inst of instr ) {
        switch(inst.type) {
            case 'rect':
                x1 = Math.min(x1, inst.x)
                x2 = Math.max(x2, inst.x+inst.width)
                y1 = Math.min(y1, inst.y)
                y2 = Math.max(y2, inst.y+inst.height)
            break
            case 'circle':
                x1 = Math.min(x1, inst.cx-inst.r)
                x2 = Math.max(x2, inst.cx+inst.r)
                y1 = Math.min(y1, inst.cy-inst.r)
                y2 = Math.max(y2, inst.cy+inst.r)
            break
        }
    }

    let data = {
        x1:x1, y1:y1, w:x2-x1, h:y2-y1, instrs: instr
    }

    let selectRect = svg.append('rect')
        .data([data])
        .attr('x', scaleX(x1))
        .attr('y', scaleY(y1))
        .attr('width', scaleX(x2-x1))
        .attr('height', scaleY(y2-y1))
        .classed('multipleSelectionRect', true)
        .attr('stroke', "#555")
        .attr('stroke-width', 1)
        .attr('fill', 'rgba(0,0,0,0.1)')
        .attr("cursor", "move")
    
    // TODO add drag handler for selection
    let g:any = self.svgd3.select('.everything').node()
    let startX = 0
    let startY = 0
    let starts = []
    let startx1 = 0
    let starty1 = 0
    selectRect.call(d3.drag()
        .on('start', function(d:any) {
            let evx = d3.mouse(g)[0]
            let evy = d3.mouse(g)[1]
            startX = evx
            startY = evy
            startx1 = data.x1
            starty1 = data.y1
            starts = d.instrs.map( (e) => {
                if (e.type == "rect" ) {
                    return {x:e.x, y:e.y}
                } else if ( e.type == "circle" ) {
                    return {x:e.cx, y:e.cy}
                }
                return {x:0,y:0}
            })
        })
        .on('drag', function(d2:any) {
            let evx = d3.mouse(g)[0]
            let evy = d3.mouse(g)[1]
            for ( let i in d2.instrs ) {
                let d = d2.instrs[i]
                let s = starts[i]
                selectRect.attr('x', scaleX(startx1)+evx-startX).attr('y', scaleY(starty1)+evy-startY)
                switch(d.type) {
                    case "circle":
                      self.modifyInstruction(d[INSTRUCTION_ID], [
                        {k:'cx', v: s.x+scaleX.invert(evx-startX)}, 
                        {k:'cy', v: s.y+scaleY.invert(evy-startY)}
                      ])
                      break
                    case "rect":
                      self.modifyInstruction(d[INSTRUCTION_ID], [
                        {k:'x', v: s.x+scaleX.invert(evx-startX)}, 
                        {k:'y', v: s.y+scaleY.invert(evy-startY)}
                      ])
                      break
                  }
            }
        })
        .on('end', function(d2:any) {
            let evx = d3.mouse(g)[0]
            let evy = d3.mouse(g)[1]
            data.x1 = startx1+scaleX.invert(evx-startX)
            data.y1 = starty1+scaleY.invert(evy-startY)
            for ( let i in d2.instrs ) {
                let d = d2.instrs[i]
                let s = starts[i]
                switch(d.type) {
                    case "circle":
                      d.cx = s.x+scaleX.invert(evx-startX)
                      d.cy = s.y+scaleY.invert(evy-startY)
                      break
                    case "rect":
                      d.x = s.x+scaleX.invert(evx-startX)
                      d.y = s.y+scaleY.invert(evy-startY)
                      break
                }
            }
        })
    )
}